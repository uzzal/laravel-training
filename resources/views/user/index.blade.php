@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">User List</div>
                        <div class="pull-right">
                            <a class="btn btn-xs btn-primary" href="{{url('user/create')}}">Create</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Operations</th>
                                </tr>
                                @foreach($rows as $r)
                                <tr>
                                    <td>{{$r->name}}</td>
                                    <td>{{$r->email}}</td>
                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{url('user/edit/'.$r->id)}}">Edit</a>
                                        @if($r->is_active)
                                        <a class="btn btn-xs btn-info" href="{{url('user/activate/'.$r->id.'/false')}}">Deactivate</a>
                                        @else
                                        <a class="btn btn-xs btn-info" href="{{url('user/activate/'.$r->id.'/true')}}"><b>Activate</b></a>
                                        @endif
                                        <a class="btn btn-xs btn-danger" href="{{url('user/destroy/'.$r->id)}}">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                                </thead>

                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection