<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 9/18/2017
 * Time: 3:44 PM
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Validator;


class UserController extends Controller
{
    public function index(){
        $rows = User::all();
        return view('user.index',[
            'rows'=>$rows
        ]);
    }

    public function create(){
        return view('user.create');
    }

    public function store(Request $req){
        $this->validator($req)->validate();
        User::create([
            'name' => $req->name,
            'email' => $req->email,
            'password' => bcrypt($req->password),
        ]);

        return redirect('user');
    }

    public function edit($id){
        $row = User::find($id);
        return view('user.edit',[
            'row'=>$row
        ]);
    }

    public function validator(Request $req, $isUpdate=false)
    {
        if($isUpdate){
            $rule = [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
            ];
        }else{
            $rule = [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ];
        }

        return Validator::make($req->all(), $rule);
    }

    public function update($id, Request $req){
        $this->validator($req, true)->validate();
        User::find($id)->update($req->all());
        return redirect('user');
    }

    public function activate($id, $state){
        $state = ($state=='true')?1:0;
        User::find($id)->update(['is_active'=>$state]);
        return redirect()->back();

    }

    public function destroy($id){
        User::find($id)->delete();
        return redirect()->back();
    }

}